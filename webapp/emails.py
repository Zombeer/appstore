import requests

ELASTICEMAIL_API_KEY = "3860c7a1-c5a5-42b3-8ce6-b240035a374c"


def send_reg_notification(user, admin_mails):
    msg_info = {}
    msg_info['username'] = 'zombeer@gmail.com'
    msg_info['api_key'] = ELASTICEMAIL_API_KEY
    msg_info['from'] = 'zombeer@gmail.com'
    msg_info['from_name'] = 'Michael'
    msg_info['to'] = ';'.join(admin_mails)
    msg_info['subject'] = 'New user at Apps!'
    msg_info['body_html'] = '<h3>New user has just registered!</h3><p>{}</p><p>Please, <a href="http://78.47.185.126:5000/admin">review new account</a> and activate it asap.</p>'.format(user['Email'])
    msg_info['reply_to'] = 'zombeer@gmail.com'
    msg_info['reply_to_name'] = 'Michael'
    result = requests.post("https://api.elasticemail.com/mailer/send", data=msg_info)
    return result
