#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import csv, os
import re
import json
import bcrypt
from io import StringIO
from flask import Flask, render_template, request, make_response, session, redirect, url_for
from pymongo import MongoClient
from bson.objectid import ObjectId
from flask.ext.mail import Mail, Message
from emails import send_reg_notification
from datetime import datetime

# Configuration

app = Flask(__name__)
app.config.from_envvar('WEBAPP_CONFIG')
mongo = MongoClient(app.config['MONGO_URI'], connect=False)
db = mongo.appstore
mail = Mail(app)
app.debug = True

# Register view
@app.route("/register", methods=["GET", "POST"])
def register():
    users = db.users
    admin_mails = [x['Email'] for x in users.find({'Role': 'Admin'})]
    if request.method == "POST":
        current_user = users.find_one({"Email": request.form['Email']})
        if current_user:
            return redirect(url_for('login'))
        new_user = {"Email": request.form['Email'], "isActive": False, "Role": "User", "Password": bcrypt.hashpw(request.form['Password'].encode('utf-8'), bcrypt.gensalt())}
        new_user_id = users.insert(new_user)
        new_user['id'] = new_user
        send_reg_notification(new_user, admin_mails)
        return redirect(url_for('login'))

    return render_template('register.html')

# Log in view
@app.route("/login", methods=["GET", "POST"])
def login():
    session.clear()
    users = db.users
    if request.method == "POST":
        current_user = users.find_one({"Email": request.form['Email']})
        if bcrypt.hashpw(request.form['Password'].encode('utf-8'), current_user["Password"]) == current_user["Password"]:
            session['Email'] = request.form['Email']
            session['Role'] = current_user["Role"]
            session['isActive'] = current_user["isActive"]
            return redirect(url_for('search_results'))
        else:
            print('No pass match!')
            return render_template('login.html')
    return render_template('login.html')


# Search results view
@app.route("/", methods=['GET'])
def search_results():
    g_play = db.googleplay
    if 'Email' not in session:
        return redirect(url_for('login'))

    if session.get('isActive','False') == 'False':
        return redirect(url_for('login'))

    print(session)

    cats = ['Action', 'Adventure', 'Arcade', 'Art & Design', 'Auto & Vehicles', 'Beauty', 'Board', 'Books & Reference', 'Business', 'Card', 'Casino', 'Casual', 'Comics', 'Communication', 'Dating', 'Education', 'Educational', 'Entertainment', 'Events', 'Finance', 'Food & Drink', 'Health & Fitness', 'House & Home', 'Libraries & Demo', 'Lifestyle', 'Maps & Navigation', 'Medical', 'Music', 'Music & Audio', 'News & Magazines', 'Parenting', 'Personalization', 'Photography', 'Productivity', 'Puzzle', 'Racing', 'Role Playing', 'Shopping', 'Simulation', 'Social', 'Sports', 'Strategy', 'Tools', 'Travel & Local', 'Trivia', 'Video Players & Editors', 'Weather', 'Word']
    installs = ['500,000,000 - 1,000,000,000', '500,000 - 1,000,000', '500 - 1,000', '50,000,000 - 100,000,000', '50,000 - 100,000', '50 - 100', '5,000,000 - 10,000,000', '5,000 - 10,000', '5 - 10', '100,000,000 - 500,000,000', '100,000 - 500,000', '100 - 500', '10,000,000 - 50,000,000', '10,000 - 50,000', '10 - 50', '1,000,000,000 - 5,000,000,000', '1,000,000 - 5,000,000', '1,000 - 5,000', '1 - 5']
    content_ratings = ['USK: All ages', 'USK: Ages 6+', 'USK: Ages 12+', 'USK: Ages 18+', 'USK: Ages 16+', 'Unrated']
    android_versions = ['4.1 and up', '2.3 and up', 'Varies with device', '2.3.3 and up', '4.0 and up', '4.0.3 and up', '3.0 and up', '2.2 and up', '4.4 and up', '2.1 and up', '4.2 and up', '5.0 and up', '1.6 and up', '3.1 and up', '4.3 and up', '1.0 and up', '2.0 and up', '1.5 and up', '2.0.1 and up', '3.2 and up', '1.1 and up', '2.3 - 4.2.2', '3.0 - 4.4', '2.3.3 - 5.0', '2.3 - 5.0', '1.5 - 3.2', '2.0 - 3.1', '2.1 - 4.0.4', '2.2 - 4.2.2', '1.6 - 4.0.4', '3.0 - 5.0', '2.2 - 5.0', '1.6 - 2.1', '2.0 - 2.3.4', '2.3 - 4.4W', '2.1 - 4.4W', '2.3.3 - 4.3', '2.2 - 4.4', '2.3.3 - 4.4', '4.0 - 4.4', '2.2 - 4.0.2', '2.3 - 4.3', '4.4 - 5.0', '4.0 - 5.0', '4.0 - 4.3', '2.1 - 4.3', '2.3.3 - 4.2.2', '2.2 - 3.2', '2.1 - 4.2.2', '4.1 - 5.0', '4.0.3 - 4.4', '2.2 - 4.3', '2.2 - 2.3.4', '2.1 - 2.3.4', '2.3 - 4.4', '4.4W and up', '4.0.3 - 4.1.1', '1.6', '1.6 - 5.0', '4.0.3 - 5.0', '1.6 - 4.2.2', '2.1 - 4.4', '4.3 - 4.4W', '2.0.1 - 5.0', '1.6 - 4.1.1', '2.1 - 2.3.2', '4.1 - 4.4', '2.1 - 2.2', '2.2 - 4.0.4', '3.0 - 4.3', '3.0 - 4.0.4', '2.2 - 4.1.1', '2.1', '2.3.3 - 2.3.4', '3.0 - 4.4W', '3.0 - 4.2.2', '1.6 - 4.4', '1.5', '2.0 - 4.4', '4.3 - 4.4', '2.3 - 4.0.2', '2.2', '2.3.3 oder höher', '1.5 - 2.3.2', '3.2 - 4.4', '2.0 - 2.1', '1.6 - 2.3.4', '2.3.3 - 4.4W', '1.5 - 2.3.4', '2.3 - 2.3.4', '1.5 - 2.0.1', '2.1 - 4.0.2', '1.6 - 2.2', '3.1 - 3.2', '3.2 - 5.0', '2.2 - 3.0', '1.0 - 4.4W', '2.2 - 2.3.2', '2.1 - 3.2', '4.3', '4.1 - 4.1.1', '4.2 - 4.2.2', '2.0 - 4.1.1', '2.0.1 - 3.2', '4.0.3 - 4.4W', '4.0 - 4.1.1', '1.6 - 4.4W', '2.1 - 4.1.1', '1.0 - 4.2.2', '2.3.3 - 3.2', '2.0 - 4.0.2', '2.0.1 - 4.0.4', '3.0 - 3.2', '2.3 - 4.1.1', '1.5 - 1.6', '2.3.3 - 4.0.4', '4.0 - 4.4W', '1.0 - 4.3', '2.0 - 4.2.2', '4.3 - 5.0', '1.6 - 2.3.2', '1.5 - 2.1', '1.6 - 3.2', '1.6 - 3.0', '2.1 - 3.0', '3.1 - 4.4', '4.0.3 - 4.2.2', '2.3 - 4.0.4', '1.5 - 4.1.1', '2.0 - 4.3', '4.1 - 4.4W', '1.0 - 2.3.2', '4.1 - 4.2.2', '1.6 - 4.0.2', '1.5 - 2.2', '4.4', '4.2 - 4.4', '1.1 - 4.4', '2.3.3 - 4.1.1', '2.1 - 3.1', '1.6 - 3.1', '2.0 - 4.0.4', '4.0.3 - 4.3', '3.2 - 4.2.2', '1.5 - 5.0', '1.5 - 4.4W', '2.3 - 3.2', '3.0 - 4.1.1', '4.0 - 4.0.4', '2.1 - 5.0', '2.3 - 2.3.2', '1.5 - 4.2.2', '2.0.1 - 2.2', '2.0 - 2.2', '2.0.1 - 4.4', '4.0 - 4.2.2', '1.5 - 4.3', '1.0 - 4.1.1', '4.0.3 - 4.0.4', '2.2 - 4.4W', '4.2 - 5.0', '2.2 - 3.1', '2.0 - 4.4W', '2.1 oder höher', '1.5 - 4.0.4', '3.2 - 4.1.1', '2.0 - 3.2', '1.0 - 2.2', '1.1 - 2.1', '5.0', '4.0 - 4.0.2', '1.5 - 4.4', '1.0 - 4.4', '1.6 - 2.0.1', '2.0.1 - 2.3.4', '1.0 - 3.2', '1.6 - 4.3', '4.1 - 4.3', '2.0.1 - 4.1.1', '1.5 - 4.0.2', '2.0.1 - 2.1', '2.0 - 5.0', '2.0.1 - 3.1', '3.1 - 4.2.2', '1.1 - 4.1.1', '4.2 - 4.3', '2.2 oder höher', '1.0 - 2.3.4', '3.1 - 4.4W', '3.1 - 5.0', '1.0 - 5.0', '1.5 - 3.1']

    input_fields = dict(installs=sorted(installs, reverse=True, key=lambda value: int(''.join(re.findall('\d', value)))),
        cats=sorted(cats),
        content_ratings=sorted(content_ratings),
        android_versions=sorted(android_versions, reverse=True))

    # Getting current search params
    all_args = request.args.to_dict()
    page_num = int(all_args.get('page', 1))
    search_term = all_args.get('search', '')
    current_cat = all_args.get('category', '')
    current_av = all_args.get('androidVersion', '')
    current_installs = all_args.get('installs', '')
    current_cr = all_args.get('CR', '')
    upd_from = all_args.get('updFrom', '')
    upd_to = all_args.get('updTo', '')
    dev_address = all_args.get('devAddress', '')
    csv_export = all_args.get('csvExport', '')

    pagesize = 100

    query = {}
    if search_term:
        query["$text"] = {"$search": search_term}
    if current_cat:
        query["Category"] = current_cat
    if current_installs:
        query["Installs"] = current_installs
    if current_cr:
        query["ContentRating"] = current_cr
    if current_av:
        query["RequiresAndroid"] = current_av
    if dev_address:
        query["DeveloperAddress"] = {'$regex': re.compile(dev_address, re.IGNORECASE) }
        if not search_term:
            query["$text"] = {"$search": dev_address}
    if upd_from:
        query["Updated"] = {'$gte': datetime.strptime(upd_from, '%Y-%m-%d')}
    if upd_to:
        if upd_from:
            query['Updated']['$lte'] = datetime.strptime(upd_to, '%Y-%m-%d')
        else:
            query["Updated"] = {'$lte': datetime.strptime(upd_to, '%Y-%m-%d')}

    if csv_export:
        results = g_play.find(query, {"_id": 0, "Timeline": 0})
        si = StringIO()
        fields = ['AppName', 'AppUrl', 'Rating', 'DeveloperEmail', 'Installs', 'AppLogo', 'Updated', 'Category', 'DeveloperWebsite', 'DeveloperAddress', 'Version', 'ContentRating', 'DeveloperName', 'RequiresAndroid', 'InAppProducts', 'Description', 'RatingCount', 'CrawlDateTime']
        cw = csv.DictWriter(si, fieldnames=fields)
        cw.writeheader()

        for item in results:
            cw.writerow(item)

        # Building csv filname from search prefs
        try:
            search_term = query["$text"]["$search"]
            del query["$text"]
        except:
            search_term = "results"

        # file_name_tail = "__"+"_".join(query.values())

        output = make_response(si.getvalue())
        output.headers["Content-Disposition"] = "attachment; filename={}.csv".format(search_term)  # + file_name_tail)
        output.headers["Content-type"] = "text/csv"
        output.headers["Charset"] = "utf-8"
        return output


    print('-----------')
    print(session['Email'], session['Role'])
    try:
        print(query)
    except:
        print('Strange query here...')

    results_count = g_play.find(query).count(True)
    results = g_play.find(query).sort('RatingCount', -1).skip(pagesize*(page_num-1)).limit(pagesize)

    max_pages = int(results_count/pagesize)+1
    current_search = {}
    current_search['category'] = current_cat
    current_search['query'] = search_term
    current_search['androidVersion'] = current_av
    current_search['installs'] = current_installs
    current_search['contentRating'] = current_cr
    current_search['devAddress'] = dev_address
    current_search['updFrom'] = upd_from
    current_search['updTo'] = upd_to

    return render_template('results.html',
        current_search=current_search,
        user_email=session['Email'],
        results=results,
        page_num=page_num,
        input_fields=input_fields,
        results_count=results_count,
        max_pages=max_pages)

# Csv export
@app.route("/csv", methods=['GET'])
def csv_export():
    return


@app.route("/admin", methods=['GET'])
def admin():
    if session.get('Role','User') == 'Admin':
        users = db.users
        all_users = users.find({}, {'Password': 0})
        return render_template('admin.html', users=all_users)
    else:
        return redirect(url_for('login'))


@app.route("/admin/<user_id>/", methods=['GET', 'POST'])
def user_admin(user_id):
    users = db.users
    if session.get('Role','User') == 'Admin':
        if request.method == "POST":
            users.update({'_id': ObjectId(user_id)}, {'$set': {'Role': request.form['Role'], 'isActive': request.form['isActive']}}, upsert=False)

        user = users.find_one({'_id': ObjectId(user_id)})
        roles = ['User', 'Admin']
        is_active_variants = ['True', 'False']
        return render_template('user_admin.html', user=user, roles=roles, is_active_variants=is_active_variants)
    else:
        return redirect(url_for('login'))


# Starting app
app.secret_key = 'welcome2016!'

if __name__ == "__main__":
    app.secret_key = 'another super secret key'
    app.run(host="0.0.0.0", port=int(os.getenv('PORT', 5000)))
