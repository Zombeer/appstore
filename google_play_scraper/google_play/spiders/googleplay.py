# -*- coding: utf-8 -*-
import scrapy
import re
from datetime import datetime
import pymongo

# MONGODB_SERVER = '78.47.185.126'
MONGODB_SERVER = 'localhost'
MONGODB_PORT = 27017

connection = pymongo.MongoClient(MONGODB_SERVER, MONGODB_PORT)
db = connection['appstore']
googleplay = db['googleplay']

links = googleplay.find({}, {'_id': 0, 'AppUrl': 1})
links = [x['AppUrl'] for x in links]

print(len(links), 'found in database...\nStarting crawler...')

class GoogleplaySpider(scrapy.Spider):
    name = "googleplay"
    allowed_domains = ["play.google.com"]
    start_urls = links
    start_urls = ['https://play.google.com/store/apps']
    headers = {'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8', 'Accept-language': 'en-US,en;q=0.8,ru;q=0.6,fi;q=0.4'}
    collection_body_template = 'start={}&num=60&numChildren=0&cctcss=square-cover&cllayout=NORMAL&ipf=1&xhr=1&hl=en'

    def parse(self, response):
        apps = set([response.urljoin(x) for x in response.xpath('//a[contains(@href, "details?id")]/@href').extract()])
        for app_link in apps:
            if 'reviewId' not in app_link:
                url = app_link.split('?')[0] + '?' + app_link.split('?')[1].split('&')[0] + "&hl=en"
                yield scrapy.Request(url=url, callback=self.parse, headers=self.headers)
        AppUrl = response.url
        AppName = response.xpath('//div[@class="id-app-title"]/text()').extract_first()
        AppLogo = response.urljoin(response.xpath('//img[@class="cover-image"]/@src').extract_first())
        Category = response.xpath('//span[@itemprop="genre"]/text()').extract_first()
        DeveloperName = response.xpath('//span[@itemprop="name"]/text()').extract_first()
        DeveloperWebsite = response.xpath('//div[@class="content contains-text-link"]/a[contains(@href,"q=")]/@href').extract_first()
        if DeveloperWebsite:
            DeveloperWebsite = re.findall(r'q=(.+?)\&', DeveloperWebsite)[0]
        DeveloperEmail = response.xpath('//a[contains(@href,"mailto")]/@href').extract_first()
        if DeveloperEmail:
            DeveloperEmail = DeveloperEmail.replace('mailto:', '')
        Description = '\n'.join(response.xpath('//div[@class="show-more-content text-body"]//text()').extract()).strip()
        DeveloperAddress = response.xpath('//div[@class="content physical-address"]/text()').extract_first()
        try:
            RatingCount = int(response.xpath('//span[@class="rating-count"]/text()').extract_first().replace(',', '').replace('.', ''))
        except:
            RatingCount = 0
        try:
            Rating = float(response.xpath('//meta[@itemprop="ratingValue"]/@content').extract_first())
        except:
            Rating = float(0)
        try:
            Updated = datetime.strptime(response.xpath('//div[@itemprop="datePublished"]/text()').extract_first(), '%B %d, %Y')
        except:
            Updated = ""
        Installs = response.xpath('//div[@itemprop="numDownloads"]/text()').extract_first()
        Version = response.xpath('//div[@itemprop="softwareVersion"]/text()').extract_first()
        RequiresAndroid = response.xpath('//div[@itemprop="operatingSystems"]/text()').extract_first()
        ContentRating = response.xpath('//div[@itemprop="contentRating"]/text()').extract_first()
        InAppProducts = response.xpath('//div[text()="In-app Products"]/following-sibling::div/text()').extract_first()
        CrawlDateTime = datetime.now()

        item = dict(Category=Category,
                AppName=AppName,
                Installs=Installs,
                DeveloperName=DeveloperName,
                DeveloperAddress=DeveloperAddress,
                DeveloperEmail=DeveloperEmail,
                DeveloperWebsite=DeveloperWebsite,
                AppLogo=AppLogo,
                Rating=Rating,
                AppUrl=AppUrl,
                RequiresAndroid=RequiresAndroid,
                InAppProducts=InAppProducts,
                Updated=Updated,
                RatingCount=RatingCount,
                ContentRating=ContentRating,
                Version=Version,
                Description=Description,
                CrawlDateTime=CrawlDateTime)

        for key in item.keys():
            try:
                item[key] = item[key].strip()
            except:
                pass
        if item['AppUrl'] == 'https://play.google.com/store/apps':
            return

        yield item
