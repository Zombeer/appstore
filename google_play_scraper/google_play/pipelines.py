# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html


# item = dict(Category=Category,
#                 AppName=AppName,
#                 Installs=Installs,
#                 DeveloperName=DeveloperName,
#                 DeveloperAddress=DeveloperAddress,
#                 DeveloperEmail=DeveloperEmail,
#                 DeveloperWebsite=DeveloperWebsite,
#                 AppLogo=AppLogo,
#                 Rating=Rating,
#                 AppUrl=AppUrl,
#                 RequiresAndroid=RequiresAndroid,
#                 InAppProducts=InAppProducts,
#                 Updated=Updated,
#                 RatingCount=RatingCount,
#                 ContentRating=ContentRating,
#                 Version=Version,
#                 Description=Description,
#                 CrawlDateTime=CrawlDateTime)



import pymongo
from scrapy.conf import settings

# MONGODB_SERVER = '78.47.185.126'
MONGODB_SERVER = 'localhost'
MONGODB_PORT = 27017


class MongoDBPipeline(object):

    def __init__(self):
        connection = pymongo.MongoClient(MONGODB_SERVER)
        db = connection['appstore']
        self.collection = db['googleplay']

    def process_item(self, item, spider):

        self.collection.update_one({
              'AppUrl': item['AppUrl']
            }, {
              '$set': {
                'Category': item['Category'],
                'AppName': item['AppName'],
                'Installs': item['Installs'],
                'DeveloperName': item['DeveloperName'],
                'DeveloperAddress': item['DeveloperAddress'],
                'DeveloperEmail': item['DeveloperEmail'],
                'DeveloperWebsite': item['DeveloperWebsite'],
                'AppLogo': item['AppLogo'],
                'Rating': item['Rating'],
                'RequiresAndroid': item['RequiresAndroid'],
                'InAppProducts': item['InAppProducts'],
                'Updated': item['Updated'],
                'RatingCount': item['RatingCount'],
                'ContentRating': item['ContentRating'],
                'Version': item['Version'],
                'Description': item['Description'],
                'CrawlDateTime': item['CrawlDateTime'],
                'Timeline.' + str(round(item['CrawlDateTime'].timestamp())): {'Rating': item['Rating'], 'RatingCount': item['RatingCount'], 'Installs': item['Installs']}
              }
            }, upsert=True)


        return item


class GooglePlayPipeline(object):
    def process_item(self, item, spider):
        return item
