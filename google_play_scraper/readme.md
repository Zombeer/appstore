-==Google Play scraper==-

Usage:

Simple run:

    scrapy crawl googleplay -o {outputfilename.csv}

Run with persistent queue:

    scrapy crawl googleplay -s JOBDIR=crawls/googleplay --loglevel=ERROR --logfile log.txt

JOBDIR can vary, it's just a directory to store session queue and information files.
To stop crawler use Ctrl+C once (Hitting it twice may result to loosing some info, but will stop crawler immediately)
To resume crawling from saved point just run again:

    scrapy crawl googleplay -o {outputfilename.csv} -s JOBDIR=crawls/googleplay
