#!/bin/bash
# Remote WebApp configuration.

export WEBAPP_CONFIG=${PWD}/webapp/conf_local.py
echo "Starting WebApp with LOCAL database configuration..."
python3 ${PWD}/webapp/webapp.py