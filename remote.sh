#!/bin/bash
# Remote WebApp configuration.

export WEBAPP_CONFIG=${PWD}/webapp/conf_remote.py
echo "Starting WebApp with REMOTE database configuration..."
python3 ${PWD}/webapp/webapp.py